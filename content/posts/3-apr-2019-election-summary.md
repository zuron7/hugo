---
title: "#3 - Elections and Voting 101 - For first time voters "
date: 2019-04-01T06:06:33.000+00:00
toc: true
featuredImg: ''
tags:
- india
- elections
- lok sabha
- mp
- bangalore

---
With the elections coming up, thought I'd help out with sources for information.
I believe in first hand information. Hence the links with sources to a lot of
raw information.

First, a video on the EVM and verifying your vote.

<iframe width="560" height="315" src="https://www.youtube.com/embed/QtZIlxw871I" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Now to finding the list of candidates who are standing in your constituency.

* Bangalore North: https://kgis.ksrsac.in/election/ParliamentEng.aspx?Parlcd=24#
* Bangalore South: https://kgis.ksrsac.in/election/ParliamentEng.aspx?Parlcd=26#
* Bangalore Central: https://kgis.ksrsac.in/election/ParliamentEng.aspx?Parlcd=25#
* Bangalore Rural: https://kgis.ksrsac.in/election/ParliamentEng.aspx?Parlcd=23#
* Party Wise List: http://www.elections.in/karnataka/parliamentary-constituencies/candidate-list.html

Other constituencies can be found on this website by clicking the relevant area
on the map. To find out information on the candidate standing in your area,
I use the following sources:

* http://www.myneta.info
* https://www.prsindia.org/mptrackmap/index.html
* https://www.mplads.gov.in/mplads/AuthenticatedPages/Reports/Citizen/rptCitizenAssmblyAgencyDetails.aspx
* https://affidavit.eci.gov.in/
* MyNeta - Personal Information
* PRSIndia - Information about participation in debates in Parliament.
* MPlads - To find out what your current MP used his MPLAD funds for.
* Check the affidavits of the MP's standing for elections.

The same data has been neatly collated by B.PAC and Citizen Matters on their website.

* https://bpac.in/election-habba-2019/mp-lad-fund-utilization-study/
* https://bpac.in/election-habba-2019/
* http://citizenmatters.in/lok-sabha-election-2019

These should give you sufficient information about your existing MP
and whether he is worthy of being re-elected. For newer candidates and independents,
getting information is challenging, so I tend to use their independent
social media pages, albeit, with a grain of salt.

Twitter and Facebook are the primary social media used by politicians. BJP
candidates tend to be one up and have a website for each of their candidates.
Makes it much easier to find information about each of them.

Newspapers tend to be the best source of information for local candidates.
They tend to have an editorial page about each candidate a few days before
the election is going to take place.

Hope to see a great turnout, and hope this was of some use to you!