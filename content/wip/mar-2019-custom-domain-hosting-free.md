---
title: "Affordable E-Mail Hosting for Custom Domains"
date: 2019-03-26T11:36:33+05:30
draft: false
toc: true
featuredImg: ""
tags: 
  - email
  - hosted
  - custom domain
---

As an enthusiast self hoster, I would ideally love to self host most of my e-mail,
but the time requirement for maintainence keeps me away. A second alternative till
I start self hosting is to use a hosted service. Here's a list of good services
that don't burn the pocket, especially for one living in a developing country. 

- Yandex Mail for Domains (Free)
- Zoho Mail (Free)
	- Web Access only :(
- Mailcheap
- Migadu
- mxroute