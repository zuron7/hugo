---
title: "Programming Languages"
date: 2019-03-24T22:36:33+05:30
draft: false
toc: true
featuredImg: ""
tags: 
  - programming languages
  - language usage
---

The list of programming languages that I like, in order of goodness.

1. C (use for all product oriented projects, cos fast)
2. Julia (scientific computing)
3. Python (for hacking together a service)

Want to try

1. Rust
2. Go (for web services only)

I've used and found them meh. 

1. Java
2. C++
3. MATLAB (proprietary nonsense)