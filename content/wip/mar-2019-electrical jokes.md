---
title: "Some Nice Electrical Jokes"
date: 2019-03-11T18:05:00+05:30
draft: false
toc: true
featuredImg: ""
tags: 
  - jokes
  - EE
---

Thank you to William. J. from Youtube for these. 
[https://www.youtube.com/channel/UCofbzZ4KA7XbtamTw6B2-fw]

Q: How do you make ANY device wireless? 
A: With diagonal cutters. 

I was having trouble, trying to mount a circuit board. After awhile, I just couldn’t stand it. 

I was having trouble turning AC into DC. A diode rectified the situation. 

My Op-Amps are very supportive. They only give positive feedback. 

I was assaulted, by a D size cell. It was charged with battery. 

A corroded wire was chasing me. I yelled “You’ll never catch me, you dirty copper!” 

Q: What does an engineer say, when he’s going home? A: Oscillator. (I’ll see ya later.) 

I was trying to convince a transformer to reduce it’s flux, but it was very reluctant. 

Q: Why is a surfer, like a reflected signal? A: They are both standing on waves. 

Q: What did the 74LS95 chip say? A: Same shift, different data. 

Q: What did the cashier, say to the neutron? A: For you, no charge. 

I replaced my Rolodex, with a box of relays. I keep all of my contacts in there. 

I needed to measure resistance, in a vacuum. I had to use space probes. 

I knew an electrical engineer with a quick temper. The guy had a really short fuse. 

I suspected that my antenna was drunk. Sure enough, it was loaded. 

Q: How do binary digits get to work? A: On the data bus. 

I wasn’t sure if I owned a wind turbine, until it said “I’m a big fan of yours.” 

I finally bought a 4-D printer. It’s about time. 

I knew an amazing train driver. He was a super-conductor. 

When he started working part time, he became a semi-conductor. 

Q: What did RS-232 say to USB? A: I’m not lowering myself to your level. 

I just bought a miniature dog. I call him “sub-Woofer”. 

I connected a converter, to an inverter. I let them fight it out. 

I got a job, measuring transformer flux. I’ve found a great field. 

I attached a negative wire, to a cow. I wanted ground beef.
