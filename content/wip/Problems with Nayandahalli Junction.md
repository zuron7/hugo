Problems with Nayandahalli Junction
-----------------------------------

This article aims to explore all the problems that Nayandahalli Junction is going through that is preventing the effective utilization of Namma Metro at this location.

I am writing this because despiting my work and home locations being along the metro line, I still sometimes use personal transport. The reasons for this are that the multi-modal links are not well done. 

Luckily for us, the solutions to this are very simple and can be done with a small amount of money. The realisations of this will be felt for a long time. 

1) Bus Stops

There are six bus stops at Nayandahalli Junction. All the stops are unplanned and random. These stops are far enough from the metro station to prevent people from taking the stops. 

ORR Stop 1 - From PES

Buses coming from Banashankari towards either Peenya stop here or after the road. It varies depending on the signal conditions. Walking from here to the Metro Stations includes crossing three busy roads with no defined pavements. 

ORR Stop 2 - From PES - Across Mysore Road

From here, going to the metro station does not have footpaths either. An old unused road provides some reflief from the crossing.

ORR Stop 3 - To PES - 

The bus stop is located across the Vrishabhavati River, which has high sewage content. Hence, most people do not want to use this bus stop. The only people that you will see waiting here are people who have no other option but to use the bus or adamantly want to use public transport.

Everyone else moves to using Auto Rickshaws or they wait for much longer times at the previous stop in order to catch a bus going towards Banashankari. 

Metro Stop 1; BSK Side
This is a well made stop with a lot of space. 
The lack of trees or a shelter makes it annoying to wait here in the summer. I would recommend a shelter immedeately and have some rain trees planted here which will provide the required cool for people to wait for a bus.

The frequency of buses going towards BSK is very low at this stop, but that is not a problem as there is another stop with a high enough frequency to account for it.

The problem lies in the fact that there is not pavement to get from this stop to the other stop despite the fact that it lies only 150m away.

Metro Stop 2 - Railway Line Side

This stop is towards the Vijayanagar Line. ALl buses stop here. Nothing wrong with this stop. Requires lane segregation to allow free flow of traffic.

Nayandahalli Railway Station

ORR - Nice Road

There is a limit posed on Non-Motorised transport due to the presence of NICE Road towards Banashankari. Where NICE Road crosses, there are no pavements or bicycle lanes. Instead it opens up into a clover leaf. Which is extremely unsafe to use. 

The convex mirrors that are used to provide visibility are seldom used as nobody is expecting pedestrian traffic along this stretch. Hence, the few times that I have walked along this stretch, I have decided never to walk along this stretch ever again, as everytime I have come this close to being hit by another vehicle.	