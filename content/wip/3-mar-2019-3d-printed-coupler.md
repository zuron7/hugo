---
title: "#3 - 3D Printed Coupler for a mecanum wheel"
date: 2019-03-26T06:06:33.000+00:00
toc: true
featuredImg: ''
tags:
- 3d printing
- cad
draft: true

---
I recently had the opportunity to help out a few friends with a small 3D
printed part. It was a coupler for a [Meccanum Wheel][Insert Link Here]. 

Having never 3D printed a part before I thought I'd help them out and gain
some experience with 3D printing along the way. 

##The Problem

The shape of the motor shaft and the shape of the coupling joint in the
wheel were different. Hence requiring a change in either. 

## Solutions

The first solution that they tried was to machine the motor shaft down to the
shape of the coupler. As can be seen in the image below. 

This turned out to be a bad idea as machining a shaft attached to a motor is
not a trivial task. This led to slipping while using the motor on the robot. 
Hence getting us to the point where we need to design a custom part. 

## Methodology

The initial basic design was done in Tinkercad, because of the ease of inserting
basic shapes. This was the result of that. 

After that I exported the same to Fusion360, then created ridges along the exterior
of the coupler for additional grip. I made sure to provide a little extra material
on the outside to account for shrinkage. The extra was filed down to provide a
perfect fit within the wheel. 


Here's an image of the end result. 

When things can go wrong, they do. In this case, the inner part of the coupler
shrank down to 5.7 mm from 6mm in the STL file. I thought that the 3D printer settings accounts and 
provides for any shirnkages, but that wasn't the case. So another batch had to be printed 
with an Internal Diameter of 6.3 mm. Success!

If you wish to print this, get the files on the following places. 

[GrabCAD]
[Thingiverse]